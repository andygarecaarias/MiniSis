/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.database;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.umss.sisii.minisis.model.Score;
import static org.junit.Assert.*;

/**
 *
 * @author anthony
 */
public class DBManagerTest {
    
    private DBManager instance;
    private int id;
    
    @Before
    public void setUp() {
        id = 1012;
        instance = DBManager.getInstance();
    }

    @Test
    public void testSomeMethod() {
        List<Score> expected = new ArrayList<>();
        Score first = new Score();
        first.setScore(67);
        first.setTask("tarea del login");
        Score second = new Score();
        second.setScore(47);
        second.setTask("tarea 2");
        expected.add(first);
        expected.add(second);
        assertEquals(expected, instance.getScores(id));
    }
}
